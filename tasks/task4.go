package main

import (
	"fmt"
	"strconv"
)

func main() {
	MySquareRoot(10, 12)
}

func MySquareRoot(num, precision float64) (result float64) {
	var devision float64 = num / 2
	var temp float64

	for {
		temp = devision
		devision = (temp + (num / temp)) / 2
		if (temp - devision) == 0 {
			break
		}
	}
	h := fmt.Sprintf("%.12f", devision)
	if s, err:= strconv.ParseFloat(h,64); err == nil {
		fmt.Println(s)
	}
	return
}
