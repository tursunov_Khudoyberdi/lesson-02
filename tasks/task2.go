package main

import (
	"fmt"
	"time"
)

func main() {
	dobStr := "04.11.2000" // Replace this date with your birthday
	givenDate, err := time.Parse("02.01.2006", dobStr)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s was %s!\n", givenDate.Format("02-01-2006"), FindWeekday(givenDate))
}

func FindWeekday(date time.Time) (weekday string) {
	return date.Weekday().String()
}
