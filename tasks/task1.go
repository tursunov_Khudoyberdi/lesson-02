package main

import "fmt"

func main() {
	for i := 1; i <= 100; i++ {
		FizzBuzz(i)
	}
}

func FizzBuzz(a int) (result string) {
	if a%3 == 0 {
		fmt.Println("Fizz")
	}
	if a%5 == 0 {
		fmt.Println("Buzz")
	}
	if a%3 == 0 && a%5 == 0 {
		fmt.Println("FizzBuzz")
	}
	return
}
